from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.db.utils import IntegrityError

from django.views.decorators.csrf import csrf_exempt
from django_extensions.db.fields import json

from token_auth.models import Token
from utils.exception_catching import catch_exceptions


@csrf_exempt
@catch_exceptions
def login(request):
    body = json.loads(request.body)
    username = body['username']
    password = body['password']
    user = authenticate(
        username=username,
        password=password)

    if user:
        token = Token.objects.create(user=user)

        return JsonResponse({
            'message': 'You have logged in successfully',
            'auth_token': token.key
        })
    else:
        return JsonResponse({
            'message': 'Login details are incorrect'},
            status=403)


@csrf_exempt
@catch_exceptions
def register(request):
    body = json.loads(request.body)
    username = body['username']
    password = body['password']
    try:
        User.objects.create_user(
            username=username,
            password=password
        )
    except IntegrityError:
        return JsonResponse({
            'message': 'This username already exists!'},
            status=400)

    return JsonResponse({
        'message': 'You have registered successfully',
    })

