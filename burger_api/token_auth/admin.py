from django.contrib import admin

from token_auth.models import Token


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'key')

