import binascii
import os

from django.contrib.auth.models import User
from django.db import models

from django.utils.translation import ugettext_lazy as _


class Token(models.Model):
    key = models.CharField(
        _('Key'),
        max_length=40,
        primary_key=True
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name=_('auth_user'),
        verbose_name=_('Auth User'),
        default=None
    )

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = _('Token')
        verbose_name_plural = _('Tokens')

