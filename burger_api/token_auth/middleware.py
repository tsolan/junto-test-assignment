from django.utils.deprecation import MiddlewareMixin

from token_auth.models import Token


class TokenMiddleware(MiddlewareMixin):
    """
    Authentication Middleware for token auth.
    """
    def process_request(self, request):
        key = request.META.get('HTTP_AUTHORIZATION', '')
        if Token.objects.filter(key=key).exists():
            user = Token.objects.get(key=key).user
            request.user = user
            return None

        return None

