from django.conf.urls import url
from django.contrib import admin
from django.views.static import serve
from burger_api import settings
from menu.views import get_menu, make_order
from token_auth.views import login, register

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', login),
    url(r'^register/', register),
    url(r'^menu/$', get_menu),
    url(r'^order/$', make_order),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),

]
