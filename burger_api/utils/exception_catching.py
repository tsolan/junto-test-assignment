from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist


def catch_exceptions(func):
    """
    Decorator for validating view data by catching common exceptions
    :param func: view function to catch exceptions
    :return: resulted validated response or appropriate error message
    """
    def wrap(request, *args, **kwargs):
        try:
            valid_response = func(request, *args, **kwargs)
            return valid_response

        except KeyError as e:
            return JsonResponse(
                {'message': 'Please provide data for ' + str(e)},
                status=400)

        except ValueError as e:
            return JsonResponse(
                {'message': 'Please provide valid data for' + str(e)},
                status=400)

        except ObjectDoesNotExist as e:
            return JsonResponse(
                {'message': str(e)},
                status=404)

        except Exception:
            return JsonResponse(
                {'message': 'Some problem has appeared.'
                            ' Check the validity of the data or try again.'},
                status=500)

    return wrap
