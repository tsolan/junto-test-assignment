from django.http import JsonResponse


def is_auth(func):
    """
    Decorator for views that
    require authentication.
    """
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated:
            return func(request, *args, **kwargs)
        else:
            return JsonResponse(
                {'message': 'Please provide authentication details.'},
                status=401
            )
    return wrap

