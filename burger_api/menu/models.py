from django.db import models
from mptt.models import TreeForeignKey, MPTTModel
from django.contrib.auth.models import User
from django_extensions.db.fields import json

from django.utils.translation import ugettext_lazy as _


class Restaurant(models.Model):
    MOSCOW = 'MSK'
    SAINT_PETERBURG = 'SPB'

    CITIES = (
        (MOSCOW, _('Moscow')),
        (SAINT_PETERBURG, _('Saint-Petersburg')),

    )
    location = models.CharField(
        max_length=3,
        choices=CITIES,
        verbose_name=_('Location')
    )
    name = models.CharField(
        max_length=30,
        verbose_name=_('Name'),
        default=_('Abstract Burger')
    )


class Category(MPTTModel):
    name = models.CharField(
        max_length=50,
        verbose_name=_('Category name')
    )
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        default=None,
        db_index=True,
        on_delete=models.CASCADE,
    )

    def serialize_to_json(self):
        """
        :return: JSON of model-object
        """
        return json.dumps(self.serializable_object())

    def serializable_object(self):
        """
        :return: serializable object of model with any tree-depth using recursion
        """
        dishes = Dish.objects.filter(category=self)
        obj = {'name': self.name, 'dishes': [], 'subcategory': []}
        for dish in dishes:
            obj['dishes'].append({'id': dish.pk, 'name': dish.name, 'price': dish.price})
        for child in self.get_children():
            obj['subcategory'].append(child.serializable_object())
        return obj

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Dish(models.Model):
    price = models.IntegerField(verbose_name=_('Dish price'))
    name = models.CharField(
        max_length=50,
        verbose_name=_('Dish name')
    )
    category = models.ForeignKey(
        Category,
        related_name='dish_category',
        verbose_name='Category',
        on_delete=models.CASCADE,
        default=None
    )

    class Meta:
        verbose_name = 'Dish'
        verbose_name_plural = 'Dishes'


class Order(models.Model):
    PAID = 'PD'
    NOT_PAID = 'NP'
    CANCELED = 'CN'

    STATUS_CHOICES = (
        (PAID, _('Paid')),
        (NOT_PAID, _('Not paid')),
        (CANCELED, _('Canceled'))

    )

    dishes = models.ManyToManyField(
        Dish,
        related_name=_('dish'),
        verbose_name=_('Ordered dishes')
    )
    total_price = models.IntegerField(verbose_name=_('Order total price'))

    operator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name=_('order_operator'),
        verbose_name=_('Order operator'),
        default=None
    )
    creation_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('DateTime of order'),
    )
    restaurant = models.ForeignKey(
        Restaurant,
        related_name=_('restaurant'),
        verbose_name=_('Restaurant of order'),
        on_delete=models.CASCADE,
    )
    status = models.CharField(
        max_length=2,
        choices=STATUS_CHOICES,
        default=NOT_PAID,
    )

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

