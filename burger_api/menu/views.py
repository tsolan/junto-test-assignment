from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_extensions.db.fields import json

from menu.models import Category, Dish, Restaurant, Order
from utils.auth import is_auth
from utils.exception_catching import catch_exceptions


@csrf_exempt
@require_http_methods(['GET'])
@catch_exceptions
@is_auth
def get_menu(request):
    dishes = Category.objects.filter(level=0)
    menu = []
    for dish in dishes:
        menu.append(json.loads(dish.serialize_to_json()))

    return HttpResponse(json.dumps(menu), content_type='application/json')


@csrf_exempt
@require_http_methods(['POST'])
@catch_exceptions
@is_auth
def make_order(request):
    body = json.loads(request.body)
    operator = User.objects.get(pk=body['operator'])
    dishes = Dish.objects.filter(pk__in=body['dishes'])
    restaurant = Restaurant.objects.get(pk=body['restaurant'])
    total_price = dishes.aggregate(Sum('price'))['price__sum']
    order = Order.objects.create(operator=operator, restaurant=restaurant, total_price=total_price)
    order.dishes.add(*dishes)
    return JsonResponse({'message': 'Your order is created!',
                                    'total_price': order.total_price})
