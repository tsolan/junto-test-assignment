from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin

from menu.models import Category, Restaurant, Dish, Order


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('pk', 'location', 'name', )


@admin.register(Category)
class CategoryAdmin(DjangoMpttAdmin):
    list_display = ('pk', 'name', )


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'price', )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('pk', 'total_price', )
    fields = ('dishes', 'total_price', 'operator', 'restaurant', 'status', )
