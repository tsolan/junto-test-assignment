# Junto Test Assignment

API for burger restaurants

## Deployed with Heroku:
https://agile-fjord-98181.herokuapp.com

# API Methods

## url: /register
method: POST
Example Request: 
{"username": "test", "password": "test"}

Example Response: 
{"message": "You have registered successfully"}

## url: /login
method: POST
Example Request: 
{"username": "test", "password": "test"}

Example Response: 
{"message": "You have logged in successfully",
 "auth_token": "0bfbd1b4878c87e7bff17f7b404e7f2e53e24d77"}
 
## url: /menu
method: GET
Example Request:
Header name: Authorization
Header value: 0bfbd1b4878c87e7bff17f7b404e7f2e53e24d77

Example Response: 
[{"name": "Drinks", "dishes": {"id":1,"name":"water","price":15}, "subcategory": [{"name": "juices",...}]]

## url: /order
method: POST
Example Request:
Header name: Authorization
Header value: 0bfbd1b4878c87e7bff17f7b404e7f2e53e24d77
{"operator":1, "dishes":[1,2,3],"restaurant":1}

Example Response: 
{"message": "Your order is created!", "total_price": 40}
